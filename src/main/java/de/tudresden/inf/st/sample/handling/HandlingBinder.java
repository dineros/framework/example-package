package de.tudresden.inf.st.sample.handling;


import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;

/**
 * <Generated>
 */
public class HandlingBinder {

   public static void bindHandlersToClientNode(DiNeRosNode node){
       node.registerHandler("ClientTransition1", 1, HandlerFunctions.SAMPLE_FINAL_HANDLING_FUNCTION);
   }

    public static void bindHandlersToServerNode(DiNeRosNode node){
        node.registerHandler("ConnectorTransition", 1, HandlerFunctions.SAMPLE_SUCCESS_HANDLING_FUNCTION);
        node.registerHandler("ConnectorTransition", 2, HandlerFunctions.SAMPLE_FINAL_HANDLING_FUNCTION);
    }

    public static void bindHandlersToPublisherNode(DiNeRosNode node){
        node.registerHandler("SortRed", 1, HandlerFunctions.SAMPLE_COLOR_HANDLING_FUNCTION);
        node.registerHandler("SortRed", 2, HandlerFunctions.SAMPLE_FINAL_HANDLING_FUNCTION);
    }

    public static void bindHandlersToSubscriberNode(DiNeRosNode node){
        node.registerHandler("SubTransition", 1, HandlerFunctions.SAMPLE_FINAL_HANDLING_FUNCTION);
    }
}
