package de.tudresden.inf.st.sample.launch;

import de.tudresden.inf.st.pnml.engine.execution.TransitionHandlerService;
import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.BalloonCallbackStorage;
import de.tudresden.inf.st.pnml.jastadd.model.PetriNet;
import de.tudresden.inf.st.pnml.jastadd.model.PnmlParser;
import de.tudresden.inf.st.sample.handling.HandlingBinder;
import de.tudresden.inf.st.sample.nodes.ClientNode;
import org.ros.node.DefaultNodeMainExecutor;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.net.URI;

public class ClientNodeLauncher {

    // actual ros / mqtt host url is inserted based on configuration
    private static final String ROS_HOST = "localhost";
    private static final String ROS_MASTER_URI = "http://localhost:11311";
    private static final String MQTT_HOST = "localhost";

    private static final NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(ROS_HOST);
    private static final NodeMainExecutor nodeMainExecutor = DefaultNodeMainExecutor.newDefault();

    public static void main(java.lang.String[] args) {

        // Setup ROS Master
        nodeConfiguration.setMasterUri(URI.create(ROS_MASTER_URI));

        // Petri Net base data initialization
        String petriNetPath = "/nets/client-net.pnml";

        // Parse the PNML file
        PetriNet petriNet = PnmlParser.parsePnml(petriNetPath, true).get(0);

        // Initialize the Handling
        TransitionHandlerService transitionHandlerService = TransitionHandlerService.getInstance();
        BalloonCallbackStorage storage = petriNet.initializeCallbackStorage();
        transitionHandlerService.init(petriNet.getId(), storage);

        // Create the ROS nodes
        DiNeRosNode node2 = new ClientNode("ClientNode", petriNet, MQTT_HOST, "mqtt");

        // Setup handlers of the ROS nodes
        HandlingBinder.bindHandlersToClientNode(node2);

        // Start the ROS node
        new Thread(() -> nodeMainExecutor.execute(node2, nodeConfiguration)) {{start();}};

    }
}
