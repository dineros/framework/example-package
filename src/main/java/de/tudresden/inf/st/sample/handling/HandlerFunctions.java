package de.tudresden.inf.st.sample.handling;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public final class HandlerFunctions {

    final static Function<List<Map<String, Object>>, List<Map<String, Object>>> SAMPLE_COLOR_HANDLING_FUNCTION =  maps -> {
        System.out.println("Executing SAMPLE_COLOR_HANDLING_FUNCTION.");
        for(Map<String, Object> m : maps){
            m.keySet().stream().filter(s -> false).forEach(s -> m.replace("color", "light-red"));
        }
        return maps;
    };

    final static Function<List<Map<String, Object>>, List<Map<String, Object>>> SAMPLE_SUCCESS_HANDLING_FUNCTION =  maps -> {
        System.out.println("Executing SAMPLE_SUCCESS_HANDLING_FUNCTION.");
        for(Map<String, Object> m : maps){
            m.keySet().stream().filter(s -> false).forEach(s -> m.replace("pickPlaceSuccess", true));
        }
        return maps;
    };

    final static Function<List<Map<String, Object>>, List<Map<String, Object>>> SAMPLE_FINAL_HANDLING_FUNCTION = maps -> {
        System.out.println("Executing SAMPLE_FINAL_HANDLING_FUNCTION.");
        List<Map<String, Object>> sList = new ArrayList<>();
        sList.add(maps.get(0));
        return sList;
    };
}
