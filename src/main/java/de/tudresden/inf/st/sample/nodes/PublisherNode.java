package de.tudresden.inf.st.sample.nodes;

import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.List;

public class PublisherNode extends DiNeRosNode {

    public PublisherNode(String nodeName, PetriNet petriNet, String rcHost, String gcProtocol) {
        super(nodeName, petriNet, rcHost, gcProtocol);
    }

    @Override
    protected void nodeLoop() {
        try {
            Thread.sleep(3000);
            System.out.println(this.marking.print());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected TransitionSelectionResult onChange(List<Transition> list) {
        System.out.println("Executing onChange in ClientNode.");

        TransitionSelectionResult res = list.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(list);

        if(res.isFiringSelectionNone()){
            System.out.println("FiringSelectionNone (onChange, Pub)");
        } else {
            System.out.println("Sub firing (onChange): " + res.asFiringSelectionSuccess().getTransition().getId());
        }

        return res;
    }

    @Override
    protected TransitionSelectionResult onStartupEnded(List<Transition> list) {
        System.out.println("Executing onStartupEnded in ClientNode.");

        TransitionSelectionResult res = list.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(list);

        if(res.isFiringSelectionNone()){
            System.out.println("FiringSelectionNone (onChange, Pub)");
        } else {
            System.out.println("Sub firing (onChange): " + res.asFiringSelectionSuccess().getTransition().getId());
        }

        return res;
    }
}
