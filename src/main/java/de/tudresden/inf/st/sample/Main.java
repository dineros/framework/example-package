package de.tudresden.inf.st.sample;

import de.tudresden.inf.st.pnml.engine.execution.TransitionHandlerService;
import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import de.tudresden.inf.st.sample.handling.HandlingBinder;
import de.tudresden.inf.st.sample.nodes.ClientNode;
import de.tudresden.inf.st.sample.nodes.PublisherNode;
import de.tudresden.inf.st.sample.nodes.ServerNode;
import de.tudresden.inf.st.sample.nodes.SubscriberNode;
import org.ros.node.DefaultNodeMainExecutor;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMainExecutor;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * <Generated>
 */
public class Main {

    // actual ros / mqtt host url is inserted based on configuration
    private static final String ROS_HOST = "localhost";
    private static final String ROS_MASTER_URI = "http://localhost:11311";
    private static final String MQTT_HOST = "localhost";

    private static final NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic(ROS_HOST);
    private static final NodeMainExecutor nodeMainExecutor = DefaultNodeMainExecutor.newDefault();

    public static void main(java.lang.String[] args) {

        // Setup ROS Master
        nodeConfiguration.setMasterUri(URI.create(ROS_MASTER_URI));

        // Petri Net base data initialization
        List<String> petriNetPaths = new ArrayList<>();
        petriNetPaths.add("src/main/resources/nets/publisher-net.pnml");
        petriNetPaths.add("src/main/resources/nets/subscriber-net.pnml");
        // petriNetPaths.add("src/main/resources/nets/client-net.pnml");
        // petriNetPaths.add("src/main/resources/nets/server-net.pnml");

        // Parse the PNML files
        List<PetriNet> petriNets = new ArrayList<>();
        for(String petriNetPath : petriNetPaths){
            petriNets.add(PnmlParser.parsePnml(petriNetPath, false).get(0));
        }

        // Initialize the Handling
        TransitionHandlerService transitionHandlerService = TransitionHandlerService.getInstance();
        for(PetriNet petriNet : petriNets){
            BalloonCallbackStorage storage = petriNet.initializeCallbackStorage();
            transitionHandlerService.init(petriNet.getId(), storage);
        }

        // Create the ROS nodes
        DiNeRosNode node0 = new PublisherNode("PublisherNode", petriNets.get(0), MQTT_HOST, "mqtt");
        DiNeRosNode node1 = new SubscriberNode("SubscriberNode", petriNets.get(1), MQTT_HOST, "mqtt");
        // DiNeRosNode node2 = new ClientNode("ClientNode", petriNets.get(2), MQTT_HOST, "mqtt");
        // DiNeRosNode node3 = new ServerNode("ServerNode", petriNets.get(3), MQTT_HOST, "mqtt");

        // Setup handlers of the ROS nodes
        HandlingBinder.bindHandlersToPublisherNode(node0);
        HandlingBinder.bindHandlersToSubscriberNode(node1);
        // HandlingBinder.bindHandlersToClientNode(node2);
        // HandlingBinder.bindHandlersToServerNode(node3);

        // Start the ROS nodes
        new Thread(() -> nodeMainExecutor.execute(node0, nodeConfiguration)) {{start();}};

        /*try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        new Thread(() -> nodeMainExecutor.execute(node1, nodeConfiguration)) {{start();}};
        // new Thread(() -> nodeMainExecutor.execute(node2, nodeConfiguration)) {{start();}};
        // new Thread(() -> nodeMainExecutor.execute(node3, nodeConfiguration)) {{start();}};

    }
}
