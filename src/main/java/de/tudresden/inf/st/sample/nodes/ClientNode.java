package de.tudresden.inf.st.sample.nodes;

import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.List;

public class ClientNode extends DiNeRosNode {

    public ClientNode(String nodeName, PetriNet petriNet, String rcHost, String gcProtocol) {
        super(nodeName, petriNet, rcHost, gcProtocol);
    }

    @Override
    protected void nodeLoop() {
        try {
            Thread.sleep(4000);
            System.out.println(this.marking.print());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected TransitionSelectionResult onChange(List<Transition> list) {
        System.out.println("Executing onChange in ClientNode.");
        return list.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(list);
    }

    @Override
    protected TransitionSelectionResult onStartupEnded(List<Transition> list) {
        System.out.println("Executing onStartupEnded in ClientNode.");
        return list.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(list);
    }
}
