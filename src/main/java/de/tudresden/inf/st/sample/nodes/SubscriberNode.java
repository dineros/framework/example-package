package de.tudresden.inf.st.sample.nodes;

import de.tudresden.inf.st.pnml.engine.ros.DiNeRosNode;
import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.util.List;

public class SubscriberNode extends DiNeRosNode {

    public SubscriberNode(String nodeName, PetriNet petriNet, String rcHost, String gcProtocol) {
        super(nodeName, petriNet, rcHost, gcProtocol);
    }

    @Override
    protected void nodeLoop() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected TransitionSelectionResult onChange(List<Transition> list) {

        TransitionSelectionResult res = list.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(list);

        if(res.isFiringSelectionNone()){
            System.out.println("[" + nodeName + "] FiringSelectionNone (onChange)");
            return res.asFiringSelectionNone();
        } else {
            System.out.println("[" + nodeName + "] Firing (onChange): "
                    + res.asFiringSelectionSuccess().getTransition().getId());
            return res.asFiringSelectionSuccess();
        }
    }

    @Override
    protected TransitionSelectionResult onStartupEnded(List<Transition> list) {

        TransitionSelectionResult res = list.size() == 0 ?
                new FiringSelectionNone() : fireRandomEnabledTransition(list);

        if(res.isFiringSelectionNone()){
            System.out.println("[" + nodeName + "] FiringSelectionNone (onStartupEnded)");
        } else {
            System.out.println("[" + nodeName + "] Firing (onStartupEnded): "
                    + res.asFiringSelectionSuccess().getTransition().getId());
        }

        return res;
    }
}
